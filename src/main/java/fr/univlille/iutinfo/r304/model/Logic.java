package fr.univlille.iutinfo.r304.model;

import java.util.ArrayList;
import java.util.Random;

public class Logic {
    String[] members;

    public Logic(){
        this.members=new String[0];
    }

    public void addmember(String member){
        String[] temp=new String[members.length+1];
        for (int i = 0; i < members.length; i++) {
            temp[i]=members[i];
        }
        temp[temp.length-1]=member;
        this.members=temp;

    }

    public boolean listContains(String member){
        int cpt=0;
        while(cpt<members.length){
            if(members[cpt].equals(member)){
                return true;
            }
        }   
        return false;
    }

    public void RollList() {
        Random r=new Random();
        String temp;
        int var=0;
        String[] res= new String[members.length];
        for (int i = 0; i < res.length; i++) {
            res[i]="";
        }
        for (String member : members) {
            while (!res[var].isEmpty()) {
                var= r.nextInt(members.length);
            }
            res[var]=member;
            
        }
        if(res.equals(this.members)){
            this.RollList();
        }
        members= res;
    }

    public String getMembers() {
        return members.toString();
    }
}
