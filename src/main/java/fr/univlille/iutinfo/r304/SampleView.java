package fr.univlille.iutinfo.r304;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SampleView extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        VBox left = new VBox();
        Button addBtn = new Button();
        Label title = new Label("Inscrits :");
        TextField input = new TextField();
        input.setMinSize(10, 30);
        addBtn.setText("+");
        left.getChildren().addAll(title, input, addBtn);

        VBox right = new VBox();
        Button btn = new Button();
        btn.setText("Nouveau tirage !");
        Label circularTitle = new Label("Affichage circulaire");
        TextArea circularContent = new TextArea();
        circularContent.appendText("Example1");
        circularContent.appendText("\n -> Example2");
        circularContent.setPrefSize(250,100);
        Label alphaTitle = new Label("Affichage alphabétique");
        TextArea alphaContent = new TextArea();
        alphaContent.setPrefSize(250,100);
        alphaContent.appendText("Example1 -> Example2");
        right.getChildren().addAll(btn, circularTitle, circularContent, alphaTitle, alphaContent);

        HBox root = new HBox(left, right);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("Tirage au sort des cadeaux");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

