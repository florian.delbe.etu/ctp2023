package fr.univlille.iutinfo.r304;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.univlille.iutinfo.r304.model.Logic;

public class TestLogic {
    
    static Logic logic;

    @BeforeAll
    public static void setup(){
        logic= new Logic();
        logic.addmember("First");
        logic.addmember("Second");
        logic.addmember("Third");
        logic.addmember("Forth");
    }

    @Test
    public void test_Member_Ajoutee(){
        assertTrue(logic.listContains("First"));
        assertTrue(logic.listContains("Second"));
    }

    @Test
    public void test_Roll(){
        Logic temp=logic;
        temp.RollList();
        assertNotEquals(temp.getMembers(), logic.getMembers());
    }
}
